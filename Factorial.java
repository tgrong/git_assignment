// FACTORIAL.JAVA -- TEST COMMENTS ONLY
import java.util.Scanner;
// Factorial class definition here
class Factorial
{
   public static void main(String args[])
   {
      int n, c, fact = 1;
 
      System.out.println("Enter an integer to calculate it's factorial");
      //Comment to conflict taking inputs
      Scanner in = new Scanner(System.in);
 
      n = in.nextInt();
 //If the value is invalid, return an error message via SYSOUT
      if ( n < 0 )
         System.out.println("Number should be non-negative.");
      else
      {
//If the value is valid, perform the factorial operation on it
         for ( c = 1 ; c <= n ; c++ )
            fact = fact*c;
 
         System.out.println("Factorial of "+n+" is = "+fact);
      }
   }
}
